package com.animalsgarden;

import java.io.*;

public class Remove
{
    public void remove() throws IOException {

        String filename="";


        String id="";

        String type="";

        Remove now = new Remove();
        now.delete(filename,id,type);
    }
    void delete(String filename, String id, String type)
    {
        String line = null;
        Boolean status=false;
        try
        {

            File fileReader = new File(filename);
            File fileWriter = new File("temp.txt");

            // Opakowuje FileReader w BufferedReader.
            BufferedReader bufferedReader = new BufferedReader(new FileReader(fileReader));
            BufferedWriter bufferedWriter = new BufferedWriter(new FileWriter(fileWriter));


            while ((line = bufferedReader.readLine()) != null) {

                String [] arrOfStr = line.split(" ");
                if(arrOfStr[0].equals(id)) {

                    status=true;
                }
                else {
                    bufferedWriter.write(line);
                    bufferedWriter.newLine();
                }
            }
            if(status && (type.equals("owner")||type.equals("animal"))) {System.out.println("Deleted "+type+" with id="+id);}
            else if(type.equals("")){}
            else {System.out.println("Cannot find "+type+" with id="+id);}

            bufferedWriter.close();
            bufferedReader.close();
            fileReader.delete();
            fileWriter.renameTo(fileReader);
        }
        catch (Exception e)
        {
            System.out.println("Something went horribly wrong: "+e.getMessage());
        }
    }
}
