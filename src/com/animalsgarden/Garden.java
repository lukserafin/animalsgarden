package com.animalsgarden;

import java.io.BufferedReader;
import java.io.FileReader;

public class Garden {
    private Area[][] garden;
    private BufferedReader reader;
    private String line;
    private String type;
    private String symbol;
    private int x;
    private int y;

    //Constant determining the size of the garden
    protected static final int SIZE = 7;

    //Default constructor for Garden class
    public void garden() {

        garden = new Area[SIZE][SIZE];
        for(int i = 0; i<SIZE;i++){
            for(int j = 0; j<SIZE;j++) {
                if(i==0 || i==6 ){
                    System.out.print("- ");
                    if(j==6)System.out.println();

                }else if(j==0 || j==6){
                    System.out.print("| ");
                    if(j==6)System.out.println();

                }else{
                    symbol="* ";
                    try
                    {
                        reader = new BufferedReader(new FileReader("animal.txt"));

                        while ((line = reader.readLine())!= null) {

                            String [] arrOfStr = line.split(" ");
                            x=Integer.parseInt(arrOfStr[3]);
                            y=Integer.parseInt(arrOfStr[4]);
                            type=arrOfStr[1];
                            if(i==x && j==y ){
                                switch (type) {
                                    case "Turtle":
                                        symbol=("T ");
                                        break;
                                    case "Dog":
                                        symbol=("D ");
                                        break;
                                    case "Cat":
                                        symbol=("C ");
                                        break;
                                }
                            }

                        }

                        reader.close();
                    }

                    catch (Exception e)
                    {
                        System.out.println("Something went wrong: "+e.getMessage());
                    }

                    System.out.print(symbol);
                }

            }
        }


    }
}
