package com.animalsgarden;

import java.io.*;

public class Move {
    public void move() throws IOException {

        String filename = "";
        String id = "";
        int direction = 0;

        Move now = new Move();
        now.go(filename, id, direction);
    }

    void go(String filename, String id, int direction) {
        String line = null;
        Boolean move = false;
        String[] arrOfStr;
        int x;
        int y;

        if (direction <= 4 && direction >= 1) {
            try {

                File fileReader = new File(filename);
                File fileWriter = new File("temp.txt");

                // FileReader wrapper in BufferedReader.
                BufferedReader bufferedReader = new BufferedReader(new FileReader(fileReader));
                BufferedWriter bufferedWriter = new BufferedWriter(new FileWriter(fileWriter));


                while ((line = bufferedReader.readLine()) != null) {

                    arrOfStr = line.split(" ");
                    if (arrOfStr[0].equals(id)) {
                        switch (direction) {
                            case 1:
                                if ((Integer.parseInt(arrOfStr[3]) + 1) > (Garden.SIZE - 2)) {
                                    System.out.println("Fance!");
                                    bufferedWriter.write(line);
                                } else if (arrOfStr[6].equals("INACTIVE")) {
                                    System.out.println("You have to feed turtle!");
                                    bufferedWriter.write(line);
                                } else {
                                    arrOfStr[3] = String.valueOf(Integer.parseInt(arrOfStr[3]) + 1) + "";
                                    if (arrOfStr[1].equals("Turtle")) {
                                        arrOfStr[6] = "INACTIVE";
                                    }
                                    bufferedWriter.write((arrOfStr[0]) + " " + arrOfStr[1] + " " + arrOfStr[2] + " " + arrOfStr[3] + " " + arrOfStr[4] + " " + arrOfStr[5] + " " + arrOfStr[6] + " " + arrOfStr[7] + "\n");
                                    int od = (Integer.parseInt(arrOfStr[3]) - 1);
                                    System.out.println("From: x=" + Integer.parseInt(arrOfStr[4]) + "; y=" + od + "; To: x=" + Integer.parseInt(arrOfStr[4]) + "; y=" + Integer.parseInt(arrOfStr[3]));
                                }
                                break;
                            case 2:
                                if ((Integer.parseInt(arrOfStr[3]) - 1) <= 0) {
                                    System.out.println("Fance!");
                                    bufferedWriter.write(line);
                                } else if (arrOfStr[6].equals("INACTIVE")) {
                                    System.out.println("You have to feed turtle!");
                                    bufferedWriter.write(line);
                                } else {
                                    arrOfStr[3] = String.valueOf(Integer.parseInt(arrOfStr[3]) - 1);
                                    if (arrOfStr[1].equals("Turtle")) {
                                        arrOfStr[6] = "INACTIVE";
                                    }
                                    bufferedWriter.write((arrOfStr[0]) + " " + arrOfStr[1] + " " + arrOfStr[2] + " " + arrOfStr[3] + " " + arrOfStr[4] + " " + arrOfStr[5] + " " + arrOfStr[6] + " " + arrOfStr[7] + "\n");
                                    int od = (Integer.parseInt(arrOfStr[3]) + 1);
                                    System.out.println("From: x=" + Integer.parseInt(arrOfStr[4]) + "; y=" + od + "; To: x=" + Integer.parseInt(arrOfStr[4]) + "; y=" + (Integer.parseInt(arrOfStr[3])) + "");
                                }
                                break;
                            case 3:
                                if ((Integer.parseInt(arrOfStr[4]) + 1) > (Garden.SIZE - 2)) {
                                    System.out.println("Fance!");
                                    bufferedWriter.write(line);
                                } else if (arrOfStr[6].equals("INACTIVE")) {
                                    System.out.println("You have to feed turtle!");
                                    bufferedWriter.write(line);
                                } else {
                                    arrOfStr[4] = String.valueOf(Integer.parseInt(arrOfStr[4]) + 1);
                                    if (arrOfStr[1].equals("Turtle")) {
                                        arrOfStr[6] = "INACTIVE";
                                    }
                                    bufferedWriter.write((arrOfStr[0]) + " " + arrOfStr[1] + " " + arrOfStr[2] + " " + arrOfStr[3] + " " + arrOfStr[4] + " " + arrOfStr[5] + " " + arrOfStr[6] + " " + arrOfStr[7] + "\n");
                                    int od = (Integer.parseInt(arrOfStr[4]) - 1);
                                    System.out.println("From: x=" + od + "; y=" + Integer.parseInt(arrOfStr[3]) + "; To: x=" + Integer.parseInt(arrOfStr[4]) + "; y=" + (Integer.parseInt(arrOfStr[3])) + "");
                                }
                                break;
                            case 4:
                                if ((Integer.parseInt(arrOfStr[4]) - 1) <= 0) {
                                    System.out.println("Fance!");
                                    bufferedWriter.write(line);
                                } else if (arrOfStr[6].equals("INACTIVE")) {
                                    System.out.println("You have to feed turtle!");
                                    bufferedWriter.write(line);
                                } else {
                                    arrOfStr[4] = String.valueOf(Integer.parseInt(arrOfStr[4]) - 1);
                                    if (arrOfStr[1].equals("Turtle")) {
                                        arrOfStr[6] = "INACTIVE";
                                    }
                                    bufferedWriter.write((arrOfStr[0]) + " " + arrOfStr[1] + " " + arrOfStr[2] + " " + arrOfStr[3] + " " + arrOfStr[4] + " " + arrOfStr[5] + " " + arrOfStr[6] + " " + arrOfStr[7] + "\n");
                                    int od = (Integer.parseInt(arrOfStr[4]) + 1);
                                    System.out.println("From: x=" + od + "; y=" + Integer.parseInt(arrOfStr[3]) + "; To: x=" + (Integer.parseInt(arrOfStr[4])) + "; y=" + (Integer.parseInt(arrOfStr[3])) + "");
                                }
                                break;
                        }


                    } else {
                        bufferedWriter.write(line);
                        bufferedWriter.newLine();
                    }
                }


                bufferedWriter.close();
                bufferedReader.close();
                fileReader.delete();
                fileWriter.renameTo(fileReader);
            } catch (Exception e) {
                System.out.println("Something went wrong: " + e.getMessage());
            }
        }else{
            System.out.println("Incorrect direction. Try again!");
        }
    }
}
