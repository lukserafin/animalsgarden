package com.animalsgarden;

public interface Saveable {

    /**
     * Zwraca zestaw danych do zapisu o danym obiekcie
     *
     * @return dane
     */
    String getDataToSave();

    /**
     * zwraca nazwę pliku do którego chcemy zapisywać dane wybranej klasy
     *
     * @return nazwa pliku
     */
    String getFileName();
}