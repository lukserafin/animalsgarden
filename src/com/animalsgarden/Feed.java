package com.animalsgarden;

import java.io.*;

public class Feed
{
    private String filename="";
    private String id="";
    private String type="";


    public void feed() throws IOException {

        Feed now = new Feed();
        now.eat(filename,id,type);
    }
    void eat(String filename, String id, String type)
    {
        this.filename = filename;
        this.id = id;
        this.type = type;

        String line = null;
        Boolean status=false;
        String [] arrOfStr;
        try
        {
            // FileReader reads a text file
            File fileReader = new File(filename);
            File fileWriter = new File("temp.txt");

            // FileReader wrapper in BufferedReader.
            BufferedReader bufferedReader = new BufferedReader(new FileReader(fileReader));
            BufferedWriter bufferedWriter = new BufferedWriter(new FileWriter(fileWriter));


            while ((line=bufferedReader.readLine()) != null) {
                arrOfStr = line.split(" ");


                if(arrOfStr[0].equals(id) && arrOfStr[6].equals("INACTIVE") && arrOfStr[1].equals(type)) {
                    line=arrOfStr[0]+" "+arrOfStr[1]+" "+arrOfStr[2] + " "+arrOfStr[3] +" "+arrOfStr[4]+" "+arrOfStr[5]+" "+"ACTIVE"+" "+arrOfStr[7]+"\n";
                    bufferedWriter.write(line);
                    status=true;
                }
                else {
                    bufferedWriter.write(line);
                    bufferedWriter.newLine();
                }
            }
            if(status) System.out.println("Feeded "+type+" with id="+id); else System.out.println("Cannot find hungry turtle with id="+id);


            bufferedReader.close();
            fileReader.delete();
            bufferedWriter.close();
            fileWriter.renameTo(fileReader);
        }
        catch (Exception e)
        {
            System.out.println("Something went wrong: "+e.getMessage());
        }
    }
}
