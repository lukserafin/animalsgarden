package com.animalsgarden;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

public class GardenFileReader {
    public void read(Readable data) {

        String line = null;


        try {

            FileReader fileReader = new FileReader(data.getFileName());

            // FileReader wrapper in BufferedReader.
            BufferedReader bufferedReader = new BufferedReader(fileReader);


            while ((line = bufferedReader.readLine()) != null) {

                System.out.println(line);


            }
            System.out.println();
            // Close file
            bufferedReader.close();
        }
        catch(FileNotFoundException ex) {
            System.out.println(
                    "Unable to open file '" +
                            data.getFileName() + "'");
        }
        catch(IOException ex) {
            System.out.println(
                    "File error '"
                            + data.getFileName() + "'");
            ex.printStackTrace();
        }catch (Exception e){
            System.out.println("Other anoun error");
            e.printStackTrace();
        }
    }


}