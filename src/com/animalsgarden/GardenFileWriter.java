package com.animalsgarden;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;

public class GardenFileWriter {

    public void save(Saveable data){
        try (BufferedWriter bw = new BufferedWriter(new FileWriter(data.getFileName(),true))){
            bw.write(data.getDataToSave());
        } catch (IOException e) {
            System.out.println("File error " + data.getFileName());
            e.printStackTrace();
        } catch (Exception e){
            System.out.println("Other anoun error");
            e.printStackTrace();
        }
    }



}
