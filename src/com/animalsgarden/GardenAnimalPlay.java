package com.animalsgarden;

import java.util.Scanner;

public class GardenAnimalPlay {

    int nrCats;
    int nrDogs;
    int nrTurtles;
    //Garden g;

    //Default constructor for GamePlay class
    //Load list of created animals
    //Create empty garden
    public GardenAnimalPlay() {
        setNrCats(1);
        setNrDogs(1);
        setNrTurtles(1);
    }

    //Getters
    public int getNrCats() {
        return nrCats;
    }
    public int getNrDogs() { return nrDogs; }
    public int getNrTurtles() {
        return nrTurtles;
    }

    //Settery
    public void setNrCats(int nrCats) { this.nrCats = nrCats; }
    public void setNrDogs(int nrDogs) { this.nrDogs = nrDogs; }
    public void setNrTurtles(int nrTurtles) { this.nrTurtles = nrTurtles; }


    //Create object Dog
    private void createDog() {
        Scanner scanner1 = new Scanner(System.in);
        System.out.println("Enter name: ");
        String nameAnimal = scanner1.nextLine();
        int x=1;
        int y=1;
        String sexAnimal="Dog";
        Scanner scanner2 = new Scanner(System.in);
        System.out.println("Enter age: ");
        int ageAnimal = scanner2.nextInt();
        Scanner scanner3 = new Scanner(System.in);

        Scanner scanner4 = new Scanner(System.in);
        System.out.println("Enter breed:");
        String breed = scanner4.nextLine();

        System.out.println("Choose owner:");
        Readable owners = new Readable() {@Override
        public String getFileName() {
            return "owner.txt";
        }
        };
        GardenFileReader owner = new GardenFileReader();
        owner.read(owners);
        System.out.println("Id Owner:");
        int idOwner = scanner3.nextInt();
        Dog d = new Dog(nameAnimal,x,y,sexAnimal,ageAnimal,idOwner, breed);
        GardenFileWriter dogs = new GardenFileWriter();
        dogs.save(d);
    }
    //Create object Cat
    private void createCat() {
        Scanner scanner1 = new Scanner(System.in);
        System.out.println("Enter name: ");
        String nameAnimal = scanner1.nextLine();
        int x=1;
        int y=1;
        String sexAnimal="Cat";
        Scanner scanner2 = new Scanner(System.in);
        System.out.println("Enter age:");
        int ageAnimal = scanner2.nextInt();

        Scanner scanner4 = new Scanner(System.in);
        System.out.println("Enter breed:");
        String breed = scanner4.nextLine();


        Scanner scanner3 = new Scanner(System.in);
        System.out.println("Choose owner:");
        Readable owners = new Readable() {@Override
        public String getFileName() {
            return "owner.txt";
        }
        };
        GardenFileReader owner = new GardenFileReader();
        owner.read(owners);
        System.out.println("Id Owner:");
        int idOwner = scanner3.nextInt();
        Cat c = new Cat(nameAnimal,x,y,sexAnimal,ageAnimal,idOwner, breed);
        GardenFileWriter cats = new GardenFileWriter();
        cats.save(c);


    }

    //Create object Turtle
    private void createTurtle() {
        Scanner scanner1 = new Scanner(System.in);
        System.out.println("Enter name: ");
        String nameAnimal = scanner1.nextLine();
        int x=1;
        int y=1;
        String sexAnimal="Turtle";
        Scanner scanner2 = new Scanner(System.in);
        System.out.println("Enter age: ");
        int ageAnimal = scanner2.nextInt();
        Scanner scanner3 = new Scanner(System.in);
        System.out.println("Choose owner:");
        Readable owners = new Readable() {@Override
        public String getFileName() {
            return "owner.txt";
        }
        };
        GardenFileReader owner = new GardenFileReader();
        owner.read(owners);
        System.out.println("Id Owner:");
        int idOwner = scanner3.nextInt();
        Turtle t = new Turtle(nameAnimal,x,y,sexAnimal,ageAnimal,idOwner);
        t.setStatus(Turtle.Status.ACTIVE);
        GardenFileWriter turtles = new GardenFileWriter();
        turtles.save(t);


    }

    // Create Animal (starting coordinates [1,1])
    public void start() {
        int number;

        System.out.println("1.Add Dog\n2.Add Cat\n3.Add Turtle");
        Scanner scanner1 = new Scanner(System.in);
        number = scanner1.nextInt();

        switch (number) {
            case 1:
                createDog();

                break;
            case 2:
                createCat();
                break;
            case 3:
                createTurtle();
                break;
        }


    }
}
