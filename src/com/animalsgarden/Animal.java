package com.animalsgarden;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;

public abstract class Animal implements Saveable,Readable {
    protected int x, y;
    protected String nameAnimal;
    protected String sexAnimal;
    protected int ageAnimal;
    protected long idAnimal;
    protected long idOwner;

    //Default constructor for the Animal class

    public Animal() {
    }
    //Constructor for the Animal class
    public Animal(String nameAnimal, int x, int y, String sexAnimal,int ageAnimal, long idOwner) {
        this.idAnimal = sum_of_animals();
        this.nameAnimal=nameAnimal;
        this.x=x;
        this.y=y;
        this.sexAnimal=sexAnimal;
        this.ageAnimal=ageAnimal;
        this.idOwner=idOwner;
    }

    //Setters
    public void setNameAnimal(String nameAnimal) {
        this.nameAnimal = nameAnimal;
    }

    public void setX(int x) {
        this.x = x;
    }

    public void setY(int y) { this.y = y; }

    public void setSex(String sex) {
        this.sexAnimal = sex;
    }

    public void setAge(int age) {
        this.ageAnimal = age;
    }

    public void setIdOwner(long idOwner) { this.idOwner = idOwner; }


    //Getters
    public long getIdAnimal() {
        return idAnimal;
    }

    public String getNameAnimal() { return nameAnimal; }

    public int getX() { return x; }

    public int getY() {
        return y;
    }

    public String getSexAnimal() { return sexAnimal; }

    public int getAge() { return ageAnimal; }

    public long getIdOwner() { return idOwner; }

    public String getFileName() {
        return "animal.txt";
    }


    public String getDataToSave(){

        String result = (idAnimal)+" "+sexAnimal+" "+nameAnimal + " "+x +" "+y+" "+ageAnimal+" "+idOwner+"\n";
        return result;

    }

    public long sum_of_animals() {
        BufferedReader reader;
        String line;
        long lines = 1;
        try {
            reader = new BufferedReader(new FileReader(getFileName()));

            while ((line = reader.readLine())!= null) {

                String [] arrOfStr = line.split(" ");
                if(lines<=Integer.parseInt(arrOfStr[0])) {
                    lines=Integer.parseInt(arrOfStr[0])+1;
                }
            }

            reader.close();
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        return lines;
    }

    public abstract String symbol();
}
