package com.animalsgarden;

public class Cat extends Animal  implements Saveable{

    protected String breed;
    //Default constructor for Cat class
    public Cat() { super(); }

    //Constructor for Cat class
    public Cat(String nameAnimal, int x, int y,String sexAnimal,int ageAnimal,long idOwner, String breed) {
        super(nameAnimal, x, y,sexAnimal,ageAnimal,idOwner);
        this.breed=breed;
    }
    //Getters
    @Override
    public String getNameAnimal(){ return super.getNameAnimal(); }

    @Override
    public int getX() {
        return super.getX();
    }

    @Override
    public int getY() {
        return super.getY();
    }

    @Override
    public String getSexAnimal(){
        return super.getSexAnimal();
    }

    @Override
    public int getAge(){
        return super.getAge();
    }

    @Override
    public long getIdOwner() {
        return super.getIdOwner();
    }

    @Override
    public String getDataToSave(){

        System.out.println();
        System.out.println("Added Cat's information:");
        System.out.println("IdAnimal: " + idAnimal);
        System.out.println("Sex: " + sexAnimal);
        System.out.println("Name: " + nameAnimal);
        System.out.println("Set x: " + x);
        System.out.println("Set y: " + y);
        System.out.println("Age: " + ageAnimal);
        System.out.println("Breed: " + breed);
        System.out.println("IdOwner: " + idOwner);
        System.out.println();

        String result = (idAnimal)+" "+sexAnimal+" "+nameAnimal + " "+x +" "+y+" "+ageAnimal+" "+breed+" "+idOwner+"\n";
        return result;

    }

    //Symbol
    @Override
    public String symbol(){
        return " C ";
    }
}
