package com.animalsgarden;

import java.util.Scanner;

public class Menu
{

    static Scanner in = new Scanner( System.in );
    private String line;
    void start()
    {
        System.out.println("------Welcome in Animal Garden-------");

        int nrOption;
        do
        {
            showMenu();
            nrOption = in.nextInt();
            switch( nrOption )
            {
                case 1 : firstOption();
                    break;
                case 2 : secondOption();
                    break;
                case 3 : thirdOption();
                    break;
                case 4 : fourthOption();
                    break;
                case 5 : fifthOption();
                    break;
                case 6 : sixthOption();
                    break;
                case 7 : seventhOption();
                    break;
                case 8 : eighthOption();
                    break;
                case 9 : play();
                    break;
            }
        }
        while( nrOption != 10 );
    }


    static void firstOption() { System.out.println( "-----Add new owner-----" );

        Scanner scanner = new Scanner(System.in);
        System.out.println("Enter your name: ");
        String firstName = scanner.nextLine();
        System.out.println("Enter your second name: ");
        String secondName = scanner.nextLine();
        System.out.println("Enter your surname: ");
        String lastName = scanner.nextLine();
        System.out.println("Enter your age: ");
        int age = scanner.nextInt();
        Owner owner = new Owner(firstName,secondName,lastName,age);
        System.out.println("If you are man enter 1, if you are woman enter 2: ");
        int number = in.nextInt();;
        switch (number) {
            case 1:
                owner.setSex(Owner.Sex.MAN);
                break;
            case 2:
                owner.setSex(Owner.Sex.WOMAN);
                break;
        }
        GardenFileWriter owners = new GardenFileWriter();
        owners.save(owner);
        System.out.println("Owner added successfully");
    }

    static void secondOption() { System.out.println( "-----Delete owner-----" );

        System.out.println("Enter id owner to delete");
        Scanner scanner = new Scanner(System.in);
        String id_owner = scanner.nextLine();
        Remove now = new Remove();
        now.delete("owner.txt",id_owner,"owner");
    }

    static void thirdOption() { System.out.println( "-----List of owners-----" );
        Readable owners = new Readable() {
            @Override
            public String getFileName() {
                return "owner.txt";
            }
        };
        GardenFileReader owner = new GardenFileReader();
        owner.read(owners);
    }

    static void fourthOption() { System.out.println( "-----Add new animal-----" );
        GardenAnimalPlay gp=new GardenAnimalPlay();
        gp.start();
    }

    static void fifthOption() {
        System.out.println( "-----Delete animal-----" );
        System.out.println("Enter id animal to delete");
        Scanner scanner = new Scanner(System.in);
        String id_animal = scanner.nextLine();
        Remove now = new Remove();
        now.delete("animal.txt",id_animal,"animal");
    }

    static void sixthOption() {
        System.out.println( "-----List of animals-----" );
        Readable animals = new Readable() {
            @Override
            public String getFileName() {
                return "animal.txt";
            }
        };
        GardenFileReader owner = new GardenFileReader();
        owner.read(animals);
    }

    static void seventhOption() { System.out.println( "-----Move animal-----" );
        System.out.println( );
        System.out.println("Enter id animal to move");
        Scanner scanner = new Scanner(System.in);
        String id_animal = scanner.nextLine();
        System.out.println("Direction? 1-down, 2-up, 3-right, 4-left");
        Scanner scanner1 = new Scanner(System.in);
        int direction = scanner1.nextInt();


        Move action = new Move();
        action.go("animal.txt", id_animal ,direction);
    }

    static void eighthOption()
    {
        System.out.println( "-----Feed turtle-----" );

        System.out.println("Enter id of turtle to feed");
        Scanner scanner = new Scanner(System.in);
        String id_animal = scanner.nextLine();
        Feed eating = new Feed();
        eating.eat("animal.txt",id_animal,"Turtle");

    }

    static void printGarden() {
        Garden g = new Garden();
        g.garden();
    }

    static void play() {
        printGarden();
    }

    static void showMenu()
    {
        System.out.print(
                "MENU:\n" +
                        "1. Add new owner\n" +
                        "2. Delete owner\n" +
                        "3. List of owners\n" +
                        "4. Add new animal\n" +
                        "5. Delete animal\n" +
                        "6. List of animals\n" +
                        "7. Move animal\n" +
                        "8. Feed turtle\n" +
                        "9. Show visualization of garden\n" +
                        "10. End\n>> "
        );
    }
}
