package com.animalsgarden;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;

public class Owner extends EnumSex implements Saveable,Readable {
    private long idOwner;
    private String firstName;
    private String secondName;
    private String lastName;
    private Sex sex; //Kobieta / Mezczyzna
    private int age;

    public Owner(String firstName,String secondName, String lastName, int age) {
        this.idOwner = sum_of_owners();
        this.firstName = firstName;
        this.secondName = secondName;
        this.lastName = lastName;
        this.sex=getSex();
        this.age = age;
    }

    public String getDataToSave(){
        System.out.println();
        System.out.println("Added owner's information:");
        System.out.println("Id: " + idOwner);
        System.out.println("Name: " + firstName);
        System.out.println("Second Name: " + secondName);
        System.out.println("Surname: " + lastName);
        System.out.println("Sex: " + sex);
        System.out.println("Age: " + age);
        System.out.println();
        String result = (idOwner)+ " "+firstName +" "+secondName+" "+lastName + " "+sex + " "+age+"\n";
        return result;
    }

    public String getFileName(){
        return "owner.txt";
    }

    //Settery
    public void setIdOwner(long id) {
        this.idOwner = idOwner;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public void setSecondName(String secondName) {
        this.secondName = secondName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public void setSex(Sex sex) {
        this.sex = sex;
    }

    public void setAge(int age) {
        this.age = age;
    }

    //Gettery

    public long getIdOwner() {
        return idOwner;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getSecondName() {
        return secondName;
    }

    public String getLastName() {
        return lastName;
    }

    public Sex getSex() {
        return sex;
    }

    public int getAge() {
        return age;
    }

    public long sum_of_owners() {
        BufferedReader reader;
        String line;
        String [] arrOfStr;

        long lines = 1;
        try {
            reader = new BufferedReader(new FileReader(getFileName()));

            while ((line = reader.readLine())!= null) {

                arrOfStr = line.split(" ");
                if(lines<=Integer.parseInt(arrOfStr[0])) {
                    lines=Integer.parseInt(arrOfStr[0])+1;}


            }

            reader.close();
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        return lines;
    }

}

