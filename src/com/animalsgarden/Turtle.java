package com.animalsgarden;

public class Turtle extends Animal {

    public enum Status {
        ACTIVE("Aktywny"),
        HIDDEN("Schowany");

        private String valuePl;

        Status(String valuePl) {
            this.valuePl = valuePl;
        }

        public String getValuePl() {
            return valuePl;
        }

    }
    private Status status;


    //Domyślny konstruktor klasy Turtle
    public Turtle(){ super(); }

    //Konstruktor klasy Turtle
    public Turtle(String name, int x, int y, String sexAnimal, int ageAnimal, long idOwner) {
        super(name,x, y,sexAnimal,ageAnimal,idOwner);
        this.status=getStatus();
        getDataToSave();

    }

    //Gettery
    @Override
    public String getNameAnimal(){ return super.getNameAnimal(); }

    @Override
    public int getX(){
        return super.getX();
    }

    @Override
    public int getY(){
        return super.getY();
    }

    @Override
    public String getSexAnimal(){
        return super.getSexAnimal();
    }

    @Override
    public int getAge(){
        return super.getAge();
    }

    @Override
    public long getIdOwner() {
        return super.getIdOwner();
    }

    //Symbol
    @Override
    public String symbol(){
        return " T ";
    }

    public Status getStatus() {
        return status;
    }

    public void setStatus(Status status) {
        this.status = status;
    }

    @Override
    public String getDataToSave(){

        System.out.println();
        System.out.println("Added Turtle's information:");
        System.out.println("IdAnimal: " + idAnimal);
        System.out.println("Sex: " + sexAnimal);
        System.out.println("Name: " + nameAnimal);
        System.out.println("Set x: " + x);
        System.out.println("Set y: " + y);
        System.out.println("Age: " + ageAnimal);
        System.out.println("Status: " + status);
        System.out.println("IdOwner: " + idOwner);
        System.out.println();

        String result = (idAnimal)+" "+sexAnimal+" "+nameAnimal + " "+x +" "+y+" "+ageAnimal+" "+status+" "+idOwner+"\n";
        return result;

    }

}
