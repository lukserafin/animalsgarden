package com.animalsgarden;

public  class Area {
    private String area;
    private boolean empty;

    //Default constructor for Area class
    public Area(){
        this.empty=true;
    }

    //Setters
    public void setArea(String area) { this.area = area; }
    public void setEmpty(boolean empty) { this.empty = empty; }

    //Getters
    public String getArea() { return area; }
    public boolean getEmpty() { return empty; }

}
