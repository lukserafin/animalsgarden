package com.animalsgarden;

public interface Readable {

    /**
     * zwraca nazwę pliku do którego chcemy zapisywać dane wybranej klasy
     *
     * @return nazwa pliku
     */
    String getFileName();
}
