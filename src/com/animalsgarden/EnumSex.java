package com.animalsgarden;

public class EnumSex {
    public enum Sex {
        MAN("Male"),
        WOMAN("Female");

        private String valueSex;

        Sex(String valueSex) {
            this.valueSex = valueSex;
        }

        public String getValueSex() {
            return valueSex;
        }

    }
}