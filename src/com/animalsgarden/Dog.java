package com.animalsgarden;

public class Dog extends Animal implements Saveable, Readable {
    protected String breed;
    //Default constructor for Dog class
    public Dog(){ super(); }

    //Constructor for Dog class
    public Dog(String nameAnimal, int x, int y,String sexAnimal,int ageAnimal, long idOwner, String breed) {
        super(nameAnimal, x, y,sexAnimal,ageAnimal,idOwner);
        this.breed=breed;
    }

    //Getters
    @Override
    public String getNameAnimal(){ return super.getNameAnimal(); }

    @Override
    public int getX(){
        return super.getX();
    }

    @Override
    public int getY(){
        return super.getY();
    }

    @Override
    public String getSexAnimal(){
        return super.getSexAnimal();
    }

    @Override
    public int getAge(){
        return super.getAge();
    }

    @Override
    public long getIdOwner() {
        return super.getIdOwner();
    }

    @Override
    public String getDataToSave(){

        System.out.println();
        System.out.println("Added Dog's information:");
        System.out.println("IdAnimal: " + idAnimal);
        System.out.println("Sex: " + sexAnimal);
        System.out.println("Name: " + nameAnimal);
        System.out.println("Set x: " + x);
        System.out.println("Set y: " + y);
        System.out.println("Age: " + ageAnimal);
        System.out.println("Breed: " + breed);
        System.out.println("IdOwner: " + idOwner);
        System.out.println();

        String result = (idAnimal)+" "+sexAnimal+" "+nameAnimal + " "+x +" "+y+" "+ageAnimal+" "+breed+" "+idOwner+"\n";
        return result;
    }

    //Symbol
    @Override
    public String symbol(){
        return " D ";
    }
}

