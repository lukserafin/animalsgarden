Projekt Zwierzęta w ogrodzie

Do dyspozycji mamy kwadratowy ogród o rozmiarze SIZE x SIZE (gdzie 1 < SIZE < 15). W ogrodzie znajdują się zwierzęta - koty, psy i żółwie. 
Celem programu jest śledzenie ich położenie.

Każde zwierze ma swojego (jednego) właściciela, jedna osoba może posiadać kilka zwierząt.

Właściciel posiada następujące cechy:
- imię
- nazwisko
- płeć
- wiek

Każde zwierze posiada następujące cechy:
- imię
- płeć
- wiek
- aktualne położenie
- właściciel

Dodatkowo:
- koty i psy posiadają cechę "rasa" (dowolny tekst)
- żółwie posiadają cechę "stan" przyjmującą wartości: "schowany w skorupie", "aktywny"

Program powinien udostępniać następujące funkcje:
- menu tekstowe pozwalające na wybór poszczególnych opcji lub wyjście z programu.
  Przykład:

	1. Dodaj właściciela
	2. Usuń właściciela
	3. Lista właścicieli
	4. Przemieść zwierze
	5. Nakarm żółwia
	... <inne opcje> 
	0. Zakończ

- ewidencja właścicieli
	- możliwość dodania nowego właściciela
	- możliwość usunięcia istniejącego właściciela
	- możliwość wyświetlenia listy wszystkich właścicieli (wszystkie dane) wraz z listą należących do nich zwierząt

- ewidencja zwierząt
	- możliwość dodania nowego zwierzęcia
	- możliwość usunięcia istniejącego zwierzęcia
	- możliwość wyświetlenia listy wszystkich zwierząt (wszystkie dane)
	
- przemieszczanie zwierząt
	- początkową pozycją wszystkich zwierząt jest lewy górny róg ogrodu
	- w celu przesunięcia zwierzęcia najpierw wybieramy które zwierze chcemy przesunąć, a następnie wybieramy kierunek: góra, dół, lewo, prawo
	- zwierzę przesuwa się tylko o jedno pole w wybranym kierunku o ile pole docelowe mieści sie w ogrodzie
	- w przypadku próby przemieszczenia zwierzęcia na niedozwolone pole użytkownik powinien dostać odpowiedni komunikat o błędzie (nie powinno to jednak przerwać działania programu)
	- żółwie mogą się przemieszczać tylko jeśli są w stanie "aktywny"
	- po każdym przemieszczeniu żółwia jego stan zmienia się na "schowany w skorupie"
	- próba przesunięcia żółwia schowanego w skorupie powinna spowodować wyświetlenie odpowiedniego błędu
	- po udanym przemieszczeniu powinien zostać wyświetlony komunikat informujący o tym z jakiej pozycji na jaką zostało przesunięte dane zwierze

- karmienie żółwi
	- możliwość wybrania żółwia do nakarmienia - nakarmiony żółw zmienia swój status na "aktywny"
	
- zapisywanie danych
	- przed zamknięciem programu dane o wszystkich właścicielach powinny zostać zapisane do pliku "users.txt", zaś dane o zwierzętach do pliku "animals.txt"
	- przy uruchamianiu programu dane powinny być wczytywane z powyższych plików
	
- wizualizacja
	- wyświetlenie aktualnego stanu ogrodu przy pomocy znaków ASCII
